<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('currencies', function (Blueprint $table) {
            
            $table->string('country_name');
            $table->string('currency_sign');
            $table->float('one_dollar');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currencies', function (Blueprint $table) {
            
            $table->dropColumn('country_name');
            $table->dropColumn('currency_sign');
            $table->dropColumn('one_dollar');

        });
    }
}
