<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferralCutToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_payments', function (Blueprint $table) {   
            $table->integer('parent_id')->default(-1);
            $table->float('referral_cut');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_payments', function (Blueprint $table) {   
            $table->dropColumn('parent_id');
            $table->dropColumn('referral_cut');
        });
    }
}
