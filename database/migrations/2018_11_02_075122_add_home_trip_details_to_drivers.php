<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHomeTripDetailsToDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {   
            $table->integer('is_home_trip')->default(0);
            $table->string('home_address');
            $table->double('home_latitude',15,8);
            $table->double('home_longitude',15,8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {   
            $table->dropColumn('is_home_trip');
            $table->dropColumn('home_address');
            $table->dropColumn('home_latitude');
            $table->dropColumn('home_longitude');
        });
    }
}
