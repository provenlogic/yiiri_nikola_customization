@extends('layouts.admin')

@if(isset($name))
  @section('title', tr('edit_service_type'))
@else
  @section('title', 'Add a Vehicle Type')
@endif


@section('content-header', tr('service_types'))

@section('breadcrumb')
  <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>Home</a></li>
  <li><a href="{{route('admin.service.types')}}"><i class="fa fa-user"></i> {{tr('service_types')}}</a></li>
    <li class="active">Add a Vehicle Type</li>
@endsection

@section('content')

@include('notification.notify')

<style>
  @media (min-width: 768px)
  {
    .modal-dialog {
    width: 400px;
    margin: 30px auto;
  }
  }

</style>
<div class="row">

  <div class="col-md-12">

      <div class="box box-info">
          <div class="box-header">
        <div class="map_content">
            <p class="lead ">
             * Use this screen to Add a Vehicle type Ex: Sedan, Hatch back, Saloon
            </p>
             <p class="lead ">
            * The vehicle type you add here, will appear on the Passenger mobile app <a data-toggle="modal" href="#myModal">( Refer the screen shot present below the form )</a>
            </p>
          </div>
          </div>
          <!-- <div class="box-header">
            @if(isset($name))
            {{ tr('edit_service') }}
            @else
              {{ tr('create_service') }}
            @endif
          </div> -->
          <div class="panel-heading border">

          </div>

          <form class="form-horizontal bordered-group" action="{{route('admin.add.service.process')}}" method="POST" enctype="multipart/form-data" role="form">

            <div class="form-group">
              <label class="col-sm-2 control-label">Enter City</label>
              <div class="col-sm-8">
                <input type="text" id="autocomplete" onFocus="geolocate()" placeholder="Enter a location" name="model" required class="form-control">
                <div id="address">
            <input type="hidden" id="street_number" name="street_number">
                <input type="hidden" id="route" name="street_address">
                <input type="hidden" id="locality" name="city" value="{{ isset($service->city) ? $service->city : '' }}">
                <input type="hidden" id="administrative_area_level_1" name="state" value="{{ isset($service->state) ? $service->state : '' }}">
                <input type="hidden" id="postal_code" name="zip_code">
                <input type="hidden" id="country" name="country" value="{{ isset($service->country) ? $service->country : '' }}">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Vehicle Type</label>
              <div class="col-sm-8">
                <input placeholder="Eg: Sedan" type="text" name="service_name" value="{{ isset($service->name) ? $service->name : '' }}" required class="form-control">
              </div>
            </div>
            
            {{--  <div class="form-group">
              <label class="col-sm-2 control-label">Currency</label>
              <div class="col-sm-8">
              <select name="currency" value="" required class="form-control">
               <option value="">{{ tr('select') }}</option>
               @foreach($currencies as $data)
               <option value="{{$data->id}}">{{$data->currency_name}}</option>
              @endforeach
              </select>
             
              </div>
            </div> --}}
            <div class="form-group">
              <label class="col-sm-2 control-label">{{ tr('number_seats') }}</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 4" type="text" name="number_seat" value="{{ isset($service->number_seat) ? $service->number_seat : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Base Fare</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 5" type="text" name="base_fare" value="{{ isset($service->base_fare) ? $service->base_fare : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">{{ tr('minimum_fare') }}</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 10" type="text" name="min_fare" value="{{ isset($service->min_fare) ? $service->min_fare : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Booking Fee</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 0.5" type="text" name="booking_fee" value="{{ isset($service->booking_fee) ? $service->booking_fee : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tax Fee</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 0.2" type="text" name="tax_fee" value="{{ isset($service->tax_fee) ? $service->tax_fee : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">{{ tr('price_per_min') }}</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 10" type="text" name="price_per_min" value="{{ isset($service->price_per_min) ? $service->price_per_min : '' }}" required class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Price per Mile / Kms</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 5" type="text" name="price_per_unit_distance" value="{{ isset($service->price_per_unit_distance) ? $service->price_per_unit_distance : '' }}" required class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Mile -or- Kms?</label>
              <div class="col-sm-8">
              <select name="distance_unit" value="" required class="form-control">
              <option value="">{{ tr('select') }}</option>
              <option value="miles" @if(isset($service->distance_unit)) @if($service->distance_unit == 'miles') selected @endif @endif>miles</option>
              <option value="kms" @if(isset($service->distance_unit)) @if($service->distance_unit == 'kms') selected @endif @endif>kms</option>
              </select>
                <!-- <input placeholder="Eg: kms or miles" type="text" name="distance_unit" value="{{ isset($service->distance_unit) ? $service->distance_unit : '' }}" required class="form-control"> -->
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">{{ tr('picture') }}</label>
              <div class="col-sm-8">
              @if(isset($service->picture))
              <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{$service->picture}}">
              @endif
                <input name="picture" type="file">
                <p class="help-block">{{ tr('upload_message') }}</p>
              </div>
            </div>
            
             <div class="checkbox add_service_type_checkbox">
                  <label class="col-sm-2">
                    <input name="is_default" @if(isset($service)) @if($service->status ==1) checked  @else  @endif @endif  value="1"  type="checkbox">{{ tr('set_default') }}</label>
              </div>
            <input type="hidden" name="id" value="@if(isset($service)) {{$service->id}} @endif" />

            <div class="box-footer">
                <a href="{{ route('admin.service.types' ) }}" class="btn btn-danger">{{tr('cancel')}}</a>
                <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
            </div>

          </form>

      </div>

  </div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Add a Vehicle type</h4>
      </div>
      <div class="modal-body">
        <img src="../images/Vehicle-Type.png" alt="img" width="100%">
      </div>
<!--       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoujGbr86VY2F6vhh-bzZjsebCFoRn0ik&libraries=places&callback=initAutocomplete"
        async defer></script>
@endsection
