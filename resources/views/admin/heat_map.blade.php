@extends('layouts.admin')

@section('title', tr('map'))

@section('content-header','Drivers availability Stats on Map' )

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li ><i class="fa fa-map"></i> {{tr('map')}} </li>
    <li class="active"><i class="fa fa-map"></i> Driver availability Stats </li>
@endsection

@section('content')

@include('notification.notify')

<div class="panel">

    <div class="panel-body">
    
<!--         <div class="row">
            <div class="col-md-10">
                <input id="pac-input" class="controls" type="text" placeholder="Enter a location">
            </div>
            <div class="col-md-2">
                <button class="btn btn-success controls" id="location-search">Search</button>
            </div>
        </div> -->
        <div class="row">
            <div class="col-xs-12">
                <div class="map_content">
                    <p class="lead">
                         Use this screen to find the Heat Map &
                    </p>
                     
                 </div>
                <div id="map"></div>
                <div id="legend"><h3>{{ tr('providers')}}</h3>
                </div>
                <h6>*Click on a Location Pointer to view Provider Details</h6>
            </div>
        </div>
    </div>
</div>


@endsection


@section('styles')
<style type="text/css">
    
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map {
        height: 100%;
        min-height: 500px;
    }

    .controls {
        /*margin-top: 10px;*/
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        margin-bottom: 10px;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        /*margin-left: 12px;*/
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 100%;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #location-search {
        width: 100%;
    }

    #legend {
        font-family: Arial, sans-serif;
        background: rgba(255,255,255,0.8);
        padding: 10px;
        margin: 10px;
        border: 2px solid #f3f3f3;
    }
    #legend h3 {
        margin-top: 0;
        font-size: 16px;
        font-weight: bold;
        text-align: center;
    }
    #legend img {
        vertical-align: middle;
        margin-bottom: 5px;
    }
</style>
@endsection




@section('scripts')

<script >

         var map, heatmap;



      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: 12.9716, lng: 77.5946},
        });

        var analytics = {!! json_encode($analytics) !!};

        var heatmapData = analytics.map(function(entry){
            return new google.maps.LatLng(entry['latitude'], entry['longitude']);
        });  
        console.log(heatmapData);


        heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          map: map
        });

        heatmap.set('radius', heatmap.get('radius') ? null : 30);
      }

      function toggleHeatmap() {
        heatmap.setMap(heatmap.getMap() ? null : map);
      }

      function changeGradient() {
        var gradient = [
          'rgba(0, 255, 255, 0)',
          'rgba(0, 255, 255, 1)',
          'rgba(0, 191, 255, 1)',
          'rgba(0, 127, 255, 1)',
          'rgba(0, 63, 255, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(0, 0, 223, 1)',
          'rgba(0, 0, 191, 1)',
          'rgba(0, 0, 159, 1)',
          'rgba(0, 0, 127, 1)',
          'rgba(63, 0, 91, 1)',
          'rgba(127, 0, 63, 1)',
          'rgba(191, 0, 31, 1)',
          'rgba(255, 0, 0, 1)'
        ]
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
      }

     

      function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
      }

      // // Heatmap data: 500 Points
      // function getPoints() {

        

            

      //   return [
      //     new google.maps.LatLng(12.9081, 77.6476),
          
          
      //      ];
      // }


</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=visualization&callback=initMap">
</script>




@endsection
